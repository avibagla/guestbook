// server.js
// where your node app starts

// init project
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const fs = require("fs");
// const multer  = require('multer')
// const ffmpeg = require("fluent-ffmpeg");
// var storage = multer.memoryStorage()
// var upload = multer({storage:storage});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// we've started you off with Express,
// but feel free to use whatever libs or frameworks you'd like through `package.json`.

// http://expressjs.com/en/starter/static-files.html
app.use(express.static("public"));
app.use('/foundation', express.static(__dirname + '/node_modules/foundation-sites/dist/'));
app.use('/jquery', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/p5', express.static(__dirname + '/node_modules/p5/lib'));


// init sqlite db
const dbFile = "./.data/sqlite.db";
const exists = fs.existsSync(dbFile);
const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(dbFile);



const { Readable } = require('stream');

/**
 * @param binary Buffer
 * returns readableInstanceStream Readable
 */
function bufferToStream(binary) {
    const readableInstanceStream = new Readable({
      read() {
        this.push(binary);
        this.push(null);
      }
    });

    return readableInstanceStream;

  }


/*
database structure

id: integer
time: timestamp
email: text
recording: url
message: text


*/

// if ./.data/sqlite.db does not exist, create it, otherwise print records to console
db.serialize(() => {
  if (!exists) {
    db.run(
      "CREATE TABLE Messages (id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT, name TEXT)"
    );
    console.log("New table Messages created!");

    // insert default dreams
    db.serialize(() => {
      // db.run(
      //   'INSERT INTO Dreams (dream) VALUES ("Find and count some sheep"), ("Climb a really tall mountain"), ("Wash the dishes")'
      // );
    });
  } else {
    console.log('Database "Messages" ready to go!');
    db.each("SELECT * from Messages", (err, row) => {
      if (row) {
        console.log(`record: ${row.message}`);
      }
    });
  }
});

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", (request, response) => {
  response.sendFile(`${__dirname}/views/index.html`);
});

app.get("/test", (request, response) => {
  response.sendFile(`${__dirname}/views/test.html`);
});


app.get("/file", (req, res) => {
  res.sendFile(`${__dirname}/uploads/trial.mp3`)
})

// endpoint to get all the dreams in the database
app.get("/getMessages", (request, response) => {
  db.all("SELECT * from Messages", (err, rows) => {
    response.send(JSON.stringify(rows));
  });
});

// endpoint to add a dream to the database
app.post("/addMessage", (request, response, next) => {
  console.log(request.body);
  // var audio_buff = request.file;
  // console.log(audio_buff);
  // ffmpeg(bufferToStream(audio_buff.buffer))
  //   .toFormat('mp3')
  //   .on('error', (err) => {
  //     console.log('An error occurred: ' + err.message);
  //   })
  //   .on('progress', (progress) => {
  //   // console.log(JSON.stringify(progress));
  //     console.log('Processing: ' + progress.targetSize + ' KB converted');
  //   })
  //   .on('end', () => {
  //       console.log('Processing finished !');
  //   })
  //   .save("./uploads/trial.mp3");
  // DISALLOW_WRITE is an ENV variable that gets reset for new projects so you can write to the database
  if (!process.env.DISALLOW_WRITE) {
    const cleansedMessage = cleanseString(request.body.message);
    const cleansedName = cleanseString(request.body.name);

    db.run(`INSERT INTO Messages (message,name) VALUES ($message,$name)`, {"$message": cleansedMessage, "$name":cleansedName}, error => {
      if (error) {
        console.error(error);
        response.send({ message: "error!" });
      } else {
        response.send({ message: "success" });
      }
    });
  }
});

// endpoint to clear dreams from the database
// app.get("/clearDreams", (request, response) => {
//   // DISALLOW_WRITE is an ENV variable that gets reset for new projects so you can write to the database
//   if (!process.env.DISALLOW_WRITE) {
//     db.each(
//       "SELECT * from Dreams",
//       (err, row) => {
//         console.log("row", row);
//         db.run(`DELETE FROM Dreams WHERE ID=?`, row.id, error => {
//           if (row) {
//             console.log(`deleted row ${row.id}`);
//           }
//         });
//       },
//       err => {
//         if (err) {
//           response.send({ message: "error!" });
//         } else {
//           response.send({ message: "success" });
//         }
//       }
//     );
//   }
// });

// helper function that prevents html/css/script malice
const cleanseString = function(string) {
  return string.replace(/</g, "&lt;").replace(/>/g, "&gt;");
};

// listen for requests :)
var listener = app.listen(2626, () => {
  console.log(`Your app is listening on port ${listener.address().port}`);
});