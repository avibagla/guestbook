// client-side js
// run by the browser each time your view template referencing it is loaded

const dreams = [];

// define variables that reference elements on our page
const messageForm = document.forms[0];
// const dreamInput = dreamsForm.elements["dream"];
// const dreamsList = document.getElementById("dreams");
// const clearButton = document.querySelector('#clear-dreams');

// listen for the form to be submitted and add a new dream when it is

var showMessages = function(){


  console.log("in show messages")
  $.ajax({
    type:"GET",
    url:"/getMessages"
  }).then(res => {
    $(".message-container").html("");
    // console.log(res)
    var json = JSON.parse(res);
    console.log(json)
    var output_string = ""
    for (var i = json.length - 1; i >= 0; i--) {  
      output_string =  output_string + '<div class="cell small-4"> <h5 class="other-name">' + json[i]["name"] + '</h5> </div> <div class="cell small-8"> <p class="other-message">' + json[i]["message"] + '</p> </div>'
    }
    console.log(output_string)
    $(".message-container").html(output_string);
    return;
  }).then(console.log("messages added!"));
};

messageForm.onsubmit = event => {
  // stop our form submission from refreshing the page
  event.preventDefault();

  const message = messageForm.elements["message"];
  const name = messageForm.elements["name"];

  var data = {
      "message": message.value,
      "name": name.value
    };
  // data.append("audio_data", audio_message);

  $.ajax({
    type:"POST",
    url:"/addMessage",
    data: data
  })
    .then(res => {
      console.log(res);
      return;

    })
    .then(() => showMessages());

  // fetch("/addDream", {
  //   method: "POST",
  //   body: JSON.stringify(data),
  //   headers: { "Content-Type": "application/json" }
  // })
  //   .then(res => res.json())
  //   .then(response => {
  //     console.log(JSON.stringify(response));
  //   });

  // // get dream value and add it to the lis

  // reset form
  message.value = "";
  name.value = "";
};




